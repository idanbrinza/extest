import { HttpClientModule } from '@angular/common/http';
import { DocformComponent } from './docform/docform.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './signup/signup.component';
import { environment } from './../environments/environment.prod';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AngularFireModule } from '@angular/fire';
import {MatExpansionModule} from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { ClassifiedComponent } from './classified/classified.component';
import {AngularFireStorageModule} from '@angular/fire/storage';
import { CollectionClassComponent } from './collection-class/collection-class.component';

import { AngularFirestoreModule } from '@angular/fire/firestore';
/*const appRoutes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'signup', component: SignUpComponent},
  { path: 'login', component: LoginComponent},
  { path: '',
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
];
*/

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavComponent,
    LoginComponent,
    SignUpComponent,
    DocformComponent,
    ClassifiedComponent,
    CollectionClassComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    BrowserModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    AngularFireAuthModule,
    MatCardModule,
    MatSelectModule,
    MatInputModule,
    AppRoutingModule,
    AngularFireStorageModule,
    HttpClientModule,
    AngularFirestoreModule,
    

/*    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),*/
  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
