import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  private url = " https://og2yw4h297.execute-api.us-east-1.amazonaws.com/beta";

  public categories: object = { 0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech' }

  public doc: string;

  classify(): Observable<any> {
    console.log(this.doc);
    let json = {
      "articles": [
        {
          "text": this.doc
        },
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        let final = res.body.replace('[', '');
        final = final.replace(']', '');
        console.log(final);
        return final;
      })
    );
  }
  constructor(private http: HttpClient, private database: AngularFirestore) { }

  userCollection: AngularFirestoreCollection = this.database.collection('users');
  classifyCollection: AngularFirestoreCollection;

  addArticle(userId: string, classify: string, article: string) {
    const articles = { selected: classify, article: article };
    this.userCollection.doc(userId).collection('clasiify').add(articles);
  }
  getArtical(userId): Observable<any[]> {
    this.classifyCollection = this.database.collection(`users/${userId}/clasiify`);
    console.log('Articles collection created');
    return this.classifyCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    );
  }
}
