import { AuthService } from '../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignUpComponent implements OnInit {

  public errorMessage:string;
  email:string;
  password:string; 

  constructor(private authService:AuthService,
    private router:Router) {
      this.authService.getSignUpErrors().subscribe(error=>{
        this.errorMessage=error;
      });
     }


  onSubmit(){
    this.authService.SignUp(this.email,this.password);
//    if(this.errorMessage!=undefined){
//  this.router.navigate(['/welcome']);}
  }

  ngOnInit() {
  }

}
