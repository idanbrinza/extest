import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './signup/signup.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollectionClassComponent } from './collection-class/collection-class.component';

const appRoutes: Routes = [
    { path: 'welcome', component: WelcomeComponent },
    { path: 'signup', component: SignUpComponent},
    { path: 'login', component: LoginComponent},
    { path: 'classify', component: DocformComponent},
    { path: 'classified', component: ClassifiedComponent},
  { path: 'collection-classify', component: CollectionClassComponent },
    { path: '',
      redirectTo: '/welcome',
      pathMatch: 'full'
    },
  ];

  
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }