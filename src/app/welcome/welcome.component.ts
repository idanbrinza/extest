import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
userId:String;
  constructor(public authService:AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user=>{this.userId=user.uid}
      
    )
    console.log(this.userId);
  }

}
