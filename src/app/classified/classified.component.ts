import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  constructor(public classifyService:ClassifyService, public authService:AuthService, private router:Router ) { }

  category:string = "Loading.."
  selected:string;
  userId:string;

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        this.category = this.classifyService.categories[res];
        this.selected = this.category;
        
      } 
    );
    
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
    })
  }

  onSubmit(){

    this.classifyService.addArticle(this.userId,this.selected,this.classifyService.doc)
    this.router.navigate(['/collection-classify']);
  }

  

}
