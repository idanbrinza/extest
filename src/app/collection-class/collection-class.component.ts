import { ClassifyService } from './../classify.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-collection-class',
  templateUrl: './collection-class.component.html',
  styleUrls: ['./collection-class.component.css']
})
export class CollectionClassComponent implements OnInit {

  userId:string;
  articles$:Observable<any>;


  constructor(public authService:AuthService, private classifyService:ClassifyService) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
    this.articles$ = this.classifyService.getArtical(this.userId)
    })
  }
}
