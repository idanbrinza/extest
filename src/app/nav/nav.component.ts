import { AuthService } from './../auth.service';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from "@angular/common";
import { Router } from "@angular/router";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {
  title: string = 'Test';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );


  constructor(private breakpointObserver: BreakpointObserver,
    public authService: AuthService,
    location: Location,
    router: Router) {
    router.events.subscribe(val => {
      if (location.path() == "/welcome") {
        this.title = 'Welcome';
      }
      if (location.path() == "/signup") {
        this.title = 'Sign Up';
      }
      if (location.path() == "/login") {
        this.title = 'Login';
      }
      if (location.path() == "/classify") {
        this.title = 'Classify';
      }
      if (location.path() == "/collection-classify") {
        this.title = 'Collection';
      }
    });
  }
}

