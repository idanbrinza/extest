// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: "AIzaSyD2qVCMDH75xeD_pTmZUPcTdx_UJui5ji4",
  authDomain: "extest-1d617.firebaseapp.com",
  databaseURL: "https://extest-1d617.firebaseio.com",
  projectId: "extest-1d617",
  storageBucket: "extest-1d617.appspot.com",
  messagingSenderId: "119454298368",
  appId: "1:119454298368:web:13dd1a4abf7de7c8b0c3e5"

  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
